require 'test_helper'

class Admin::ComputersControllerTest < ActionController::TestCase
  setup do
    @admin_computer = admin_computers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_computers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_computer" do
    assert_difference('Admin::Computer.count') do
      post :create, admin_computer: {  }
    end

    assert_redirected_to admin_computer_path(assigns(:admin_computer))
  end

  test "should show admin_computer" do
    get :show, id: @admin_computer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_computer
    assert_response :success
  end

  test "should update admin_computer" do
    patch :update, id: @admin_computer, admin_computer: {  }
    assert_redirected_to admin_computer_path(assigns(:admin_computer))
  end

  test "should destroy admin_computer" do
    assert_difference('Admin::Computer.count', -1) do
      delete :destroy, id: @admin_computer
    end

    assert_redirected_to admin_computers_path
  end
end

require 'test_helper'

class Admin::CallsControllerTest < ActionController::TestCase
  setup do
    @admin_call = admin_calls(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_calls)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_call" do
    assert_difference('Admin::Call.count') do
      post :create, admin_call: {  }
    end

    assert_redirected_to admin_call_path(assigns(:admin_call))
  end

  test "should show admin_call" do
    get :show, id: @admin_call
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_call
    assert_response :success
  end

  test "should update admin_call" do
    patch :update, id: @admin_call, admin_call: {  }
    assert_redirected_to admin_call_path(assigns(:admin_call))
  end

  test "should destroy admin_call" do
    assert_difference('Admin::Call.count', -1) do
      delete :destroy, id: @admin_call
    end

    assert_redirected_to admin_calls_path
  end
end

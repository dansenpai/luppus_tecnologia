Rails.application.routes.draw do
  resources :services
  resources :calls, only: [:create, :show, :index, :new, :edit, :update]
  resources :computers

  devise_for :users, path: "/", path_names: { sign_in: 'entrar', sign_out: 'sair', password: 'secret', 
    confirmation: 'verificacao', unlock: 'desbloquear', registration: 'resgistro', sign_up: 'criar' }
  
  root "pages#index"
  
  get "/sistema" => "system#index"
  
  namespace :admin do 
    root "pages#index"
    get "/contrato" => "pages#contract"
    get "/relatorios"=> "pages#relatorio"
    resources :calls
    resources :computers
    resources :services

  end
end

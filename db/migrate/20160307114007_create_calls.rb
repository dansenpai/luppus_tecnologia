class CreateCalls < ActiveRecord::Migration
  def change
    create_table :calls do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.belongs_to :computer, index: true, foreign_key: true
      t.string :protocolo
      t.string :total
      t.text :description
      t.boolean :status

      t.timestamps null: false
    end
  end
end

class AddCollumnsToUser < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :phone, :string
    add_column :users, :adress, :string
    add_column :users, :number, :string
    add_column :users, :district, :string
    add_column :users, :city, :string
    add_column :users, :state, :string
    add_column :users, :job, :string
    add_column :users, :born, :datetime
    add_column :users, :avatar, :string
  end
end

class CreateComputers < ActiveRecord::Migration
  def change
    create_table :computers do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.string :name
      t.string :avatar
      t.string :cpu
      t.string :model
      t.float :ram
      t.string :brand
      t.integer :hd
      t.string :video_card
      t.string :operation_system

      t.timestamps null: false
    end
  end
end

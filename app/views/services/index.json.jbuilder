json.array!(@services) do |service|
  json.extract! service, :id, :name, :total
  json.url service_url(service, format: :json)
end

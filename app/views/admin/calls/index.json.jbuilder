json.array!(@admin_calls) do |admin_call|
  json.extract! admin_call, :id
  json.url admin_call_url(admin_call, format: :json)
end

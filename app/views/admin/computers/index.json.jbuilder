json.array!(@admin_computers) do |admin_computer|
  json.extract! admin_computer, :id
  json.url admin_computer_url(admin_computer, format: :json)
end

json.array!(@computers) do |computer|
  json.extract! computer, :id, :user_id, :name, :avatar, :cpu, :model, :ram, :brand, :hd, :video_card, :operation_system
  json.url computer_url(computer, format: :json)
end

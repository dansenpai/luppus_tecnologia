json.array!(@calls) do |call|
  json.extract! call, :id, :user_id, :computer_id, :protocolo, :total, :description, :status
  json.url call_url(call, format: :json)
end

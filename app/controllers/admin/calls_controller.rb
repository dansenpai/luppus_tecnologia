class Admin::CallsController < AdminController
  layout "admin"
  before_action :set_admin_call, only: [:show, :edit, :update, :destroy]

  # GET /admin/calls
  # GET /admin/calls.json
  def index
    @admin_calls = Call.all
  end

  # GET /admin/calls/1
  # GET /admin/calls/1.json
  def show
  end

  # GET /admin/calls/new
  def new
    @admin_call = Call.new
  end

  # GET /admin/calls/1/edit
  def edit
  end

  # POST /admin/calls
  # POST /admin/calls.json
  def create
    @admin_call = Call.new(admin_call_params)

    respond_to do |format|
      if @admin_call.save
        format.html { redirect_to @admin_call, notice: 'Call was successfully created.' }
        format.json { render :show, status: :created, location: @admin_call }
      else
        format.html { render :new }
        format.json { render json: @admin_call.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/calls/1
  # PATCH/PUT /admin/calls/1.json
  def update
    respond_to do |format|
      if @admin_call.update(admin_call_params)
        format.html { redirect_to @admin_call, notice: 'Call was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_call }
      else
        format.html { render :edit }
        format.json { render json: @admin_call.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/calls/1
  # DELETE /admin/calls/1.json
  def destroy
    @admin_call.destroy
    respond_to do |format|
      format.html { redirect_to admin_calls_url, notice: 'Call was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_call
      @admin_call = Call.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_call_params
      params.fetch(:admin_call, {})
    end
end

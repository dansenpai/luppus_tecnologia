class Admin::ComputersController < AdminController
  before_action :set_admin_computer, only: [:show, :edit, :update, :destroy]

  # GET /admin/computers
  # GET /admin/computers.json
  def index
    @admin_computers = Computer.all
  end

  # GET /admin/computers/1
  # GET /admin/computers/1.json
  def show
  end

  # GET /admin/computers/new
  def new
    @admin_computer = Computer.new
  end

  # GET /admin/computers/1/edit
  def edit
  end

  # POST /admin/computers
  # POST /admin/computers.json
  def create
    @admin_computer = Computer.new(admin_computer_params)

    respond_to do |format|
      if @admin_computer.save
        format.html { redirect_to @admin_computer, notice: 'Computer was successfully created.' }
        format.json { render :show, status: :created, location: @admin_computer }
      else
        format.html { render :new }
        format.json { render json: @admin_computer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/computers/1
  # PATCH/PUT /admin/computers/1.json
  def update
    respond_to do |format|
      if @admin_computer.update(admin_computer_params)
        format.html { redirect_to @admin_computer, notice: 'Computer was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_computer }
      else
        format.html { render :edit }
        format.json { render json: @admin_computer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/computers/1
  # DELETE /admin/computers/1.json
  def destroy
    @admin_computer.destroy
    respond_to do |format|
      format.html { redirect_to admin_computers_url, notice: 'Computer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_computer
      @admin_computer = Computer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_computer_params
      params.fetch(:admin_computer, {})
    end
end

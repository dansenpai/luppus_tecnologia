class ApplicationController < ActionController::Base
	before_action :configure_permitted_parameters, if: :devise_controller?
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def after_update_path_for(resource)
    "/sistema"
  end
  
  protected


  def configure_permitted_parameters
  		devise_parameter_sanitizer.for(:sign_up) << :name
  		devise_parameter_sanitizer.for(:account_update)<< :name

  		devise_parameter_sanitizer.for(:sign_up) << :phone
  		devise_parameter_sanitizer.for(:account_update)<< :phone

  		devise_parameter_sanitizer.for(:sign_up) << :adress
  		devise_parameter_sanitizer.for(:account_update)<< :adress

  		devise_parameter_sanitizer.for(:sign_up) << :number
  		devise_parameter_sanitizer.for(:account_update)<< :number

  		devise_parameter_sanitizer.for(:sign_up) << :district
  		devise_parameter_sanitizer.for(:account_update)<< :district

  		devise_parameter_sanitizer.for(:sign_up) << :city
  		devise_parameter_sanitizer.for(:account_update)<< :city

  		devise_parameter_sanitizer.for(:sign_up) << :state
  		devise_parameter_sanitizer.for(:account_update)<< :state

  		devise_parameter_sanitizer.for(:sign_up) << :job
  		devise_parameter_sanitizer.for(:account_update)<< :job

  		devise_parameter_sanitizer.for(:sign_up) << :born
  		devise_parameter_sanitizer.for(:account_update)<< :born

  		devise_parameter_sanitizer.for(:sign_up) << :avatar
  		devise_parameter_sanitizer.for(:account_update)<< :avatar
  end
end

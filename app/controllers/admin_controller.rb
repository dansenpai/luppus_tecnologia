class	AdminController < ApplicationController
	before_action :authenticate_user!, :is_admin?
	layout "admin"

	protected
	def is_admin?
		if	current_user.admin
			true
		else
			respond_to do |format|
				format.html {redirect_to "/", notice: "Oops, você não tem permissão para isso"}
			end
		end
	end
end
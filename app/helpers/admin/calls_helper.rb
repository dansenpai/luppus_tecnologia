module Admin::CallsHelper
		def formatted_date
			hours = DateTime.now.hour
			minutes = DateTime.now.minute
			day = DateTime.now.day
			month = DateTime.now.month
			year = DateTime.now.year

			return "#{day}/#{month}/#{year} , #{hours}:#{minutes}"
		end
end

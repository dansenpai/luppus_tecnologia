class Computer < ActiveRecord::Base
  belongs_to :user
  has_many :calls

  validates :name , presence: true
  validates :model, presence: true
  validates :user_id, presence: true
  validates :cpu, presence: true
  validates :ram, presence: true
  validates :hd, presence: true
  validates :brand, presence: true
  validates :operation_system ,presence: true
  validates :video_card, presence: true

end

class Call < ActiveRecord::Base
  belongs_to :user
  belongs_to :computer

  validates :computer_id, presence: true
  validates :description, presence: true
end

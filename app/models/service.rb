class Service < ActiveRecord::Base
	validates :name, presence: true
	validates :total, presence: true
end
